using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Domain.DTO.Project;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet("id")]
        public ActionResult<ProjectReadDTO> GetProjectById(int id)
        {
            try
            {
                ProjectReadDTO project = _projectService.GetProject(id);
                return Ok(project);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectReadDTO>> GetProjects()
        {
            return Ok(_projectService.GetAllProjects());
        }

        [HttpPost]
        public ActionResult AddProject([FromBody]ProjectCreateDTO project)
        {
            _projectService.CreateProject(project);
            return Ok();
        }

        [HttpPut]
        public ActionResult UpdateProject(ProjectUpdateDTO project)
        {
            _projectService.UpdateProject(project);
            return Ok();
        }

        [HttpDelete]
        public ActionResult DeleteProject(int id)
        {
            try
            {
                _projectService.DeleteProject(id);
                return NoContent();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }
    }
}