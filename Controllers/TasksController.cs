using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Domain.DTO.Task;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet("id")]
        public ActionResult<TaskReadDTO> GetTaskById(int id)
        {
            try
            {
                return Ok(_taskService.GetTask(id));
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskReadDTO>> GetAllTasks()
        {
            return Ok(_taskService.GetAllTasks());
        }

        [HttpPost]
        public ActionResult CreateTask(TaskCreateDTO task)
        {
            _taskService.AddNewTask(task);
            return Ok();
        }

        [HttpPut]
        public ActionResult UpdateTask(TaskUpdateDTO task)
        {
            _taskService.UpdateTask(task);
            return Ok();
        }

        [HttpDelete]
        public ActionResult DeleteTask(int id)
        {
            try
            {
                _taskService.DeleteTask(id);
                return NoContent();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }
    }
}