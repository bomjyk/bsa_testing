﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.BLL.Services;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class TDDController : ControllerBase
    {
        private readonly TDDService _service;

        public TDDController(TDDService service)
        {
            _service = service;
        }
        [HttpGet]
        public ActionResult<IEnumerable<Task>> GetUncomplitedTasksForUser(int userId)
        {
            return Ok(JsonConvert.SerializeObject(_service.GetUncomplitedTasksByUserId(userId).ToArray(), new JsonSerializerSettings()));
        }
    }
}