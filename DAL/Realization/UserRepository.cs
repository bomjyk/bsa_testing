using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.DAL.Realization
{
    public class UserRepository : IUserRepository
    {
        private readonly ProjectContext _db;
        public UserRepository(ProjectContext context)
        {
            _db = context;
        }
        public IEnumerable<User> GetAll()
        {
            return (IEnumerable<User>)_db.Users
                .Include(u => u.Team);
        }

        public User Get(int id)
        {
            User user = _db.Users
                .Include(u => u.Team)
                .FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                throw new Exception();
            }
            return user;
        }

        public void Create(User item)
        {
            _db.Users.Add(item);
        }

        public void Update(User item)
        {
            var user = _db.Users.FirstOrDefault(u => u.Id == item.Id);
            if (user != null)
            {
                user.Email = item.Email;
                user.Team = item.Team;
                user.BirthDay = item.BirthDay;
                user.FirstName = item.FirstName;
                user.LastName = item.LastName;
            }
        }

        public void Delete(User item)
        {
            _db.Users.Remove(item);
        }
    }
}