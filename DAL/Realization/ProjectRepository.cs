using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.DAL.Realization
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ProjectContext _db;

        public ProjectRepository(ProjectContext context)
        {
            _db = context;
        }
        
        public IEnumerable<Project> GetAll()
        {
            return (IEnumerable<Project>) _db.Projects
                .Include(p => p.Author)
                .Include(p => p.Team);
        }

        public Project Get(int id)
        { 
            var project = _db.Projects
                .Include(p => p.Author)
                .Include(p => p.Team)
                .FirstOrDefault(p => p.Id == id);
            if (project == null) throw new Exception();
            return project;
        }

        public void Create(Project item)
        {
            _db.Projects.Add(item);
        }

        public void Update(Project item)
        {
            var project = _db.Projects.FirstOrDefault(p => p.Id == item.Id);
            if (project != null)
            {
                project.Author = item.Author;
                project.Deadline = item.Deadline;
                project.Description = item.Description;
                project.Name = item.Name;
                project.Team = item.Team;
            }
        }

        public void Delete(Project item)
        {
            _db.Projects.Remove(item);
        }
    }
}