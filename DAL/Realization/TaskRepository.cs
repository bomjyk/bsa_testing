using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.DAL.Realization
{
    public class TaskRepository : ITaskRepository
    {
        private readonly ProjectContext _db;

        public TaskRepository(ProjectContext context)
        {
            _db = context;
        }
        
        public IEnumerable<Task> GetAll()
        {
            return (IEnumerable<Task>) _db.Tasks
                .Include(t => t.Performer)
                .Include(t => t.Performer.Team)
                .Include(t => t.Project);
        }

        public Task Get(int id)
        { 
            var task = _db.Tasks
                .Include(t => t.Performer)
                .Include(t => t.Project)
                .FirstOrDefault(t => t.Id == id);
            if (task == null) throw new Exception();
            return task;
        }

        public void Create(Task item)
        {
            _db.Tasks.Add(item);
        }

        public void Update(Task item)
        {
            var task = _db.Tasks.FirstOrDefault(t => t.Id == item.Id);
            if (task != null)
            {
                task.Description = item.Description;
                task.Name = item.Name;
                task.Performer = item.Performer;
                task.Project = item.Project;
                task.State = item.State;
                task.FinishedAt = item.FinishedAt;
            }
        }

        public void Delete(Task item)
        {
            _db.Tasks.Remove(item);
        }
    }
}