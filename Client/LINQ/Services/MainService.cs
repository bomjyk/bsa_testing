using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using ProjectStructure.Domain.DTO.LINQ;
using ProjectStructure.Domain.Entities;

namespace LINQ.Services
{
    public class MainService 
    {
        private static readonly string _apiRoute = "https://localhost:5001/api/LINQ/";
        private static readonly string _taskNumberByUserId = "taskByUserId/id?id=";
        private static readonly string _tasksListForUser = "tasksForUser/id?id=";
        private static readonly string _listOfFinishedTasksInCurrentYearForUser = "listFinishedTasks/id?id=";
        private static readonly string _sortedListTeams = "sortedListTeams";
        private static readonly string _alphabetUsersWithSortedTasks = "alphabetUsersWithSortedTasks";
        private static readonly string _userProjectAndTasks = "getUserProjectAndTasks/id?id=";
        private static readonly string _projectAndTaskStructure = "getProjectAndTaskStructure";

        private static async Task<string> GetJsonByRoute(string route)
        {
            string json;
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            using (HttpClient client = new HttpClient(clientHandler))
            {
                HttpResponseMessage tasksResponseMessage = await client.GetAsync(route);
                tasksResponseMessage.EnsureSuccessStatusCode();
                json = await tasksResponseMessage.Content.ReadAsStringAsync();
            }

            return json;
        }
        public  static Dictionary<Project, int> GetTaskNumberByUserId(int userId)
        {
            var data = JsonConvert.DeserializeObject<KeyValuePair<Project, int>[]>
                (GetJsonByRoute(_apiRoute + _taskNumberByUserId + userId).Result, new JsonSerializerSettings());
            if (data != null)
            {
                return data
                    .ToDictionary(kv => kv.Key, kv => kv.Value);
            }
            else
            {
                throw new Exception();
            }
        }

        public static List<ProjectStructure.Domain.Entities.Task> GetTasksListForUser(int userId)
        {
            return JsonConvert.DeserializeObject<List<ProjectStructure.Domain.Entities.Task>>
                (GetJsonByRoute(_apiRoute + _tasksListForUser + userId).Result);
        }

        public static List<FinishedTaskOfYearForUser> GetListOfFinishedTasksInCurrentYearForUser(int userId)
        {
            return JsonConvert.DeserializeObject<List<FinishedTaskOfYearForUser>>
                (GetJsonByRoute(_apiRoute + _listOfFinishedTasksInCurrentYearForUser + userId).Result);

        }
        public static List<TeamStructureSortedByRegistrationWithUserField> GetListOfTeamsSortedByRegistration()
        {
            return JsonConvert.DeserializeObject<List<TeamStructureSortedByRegistrationWithUserField>>
                (GetJsonByRoute(_apiRoute + _sortedListTeams ).Result);
        }

        public static List<AlphabetUserWithTask> GetUsersByAlphabetWithSortedTasks()
        {
            return JsonConvert.DeserializeObject<List<AlphabetUserWithTask>>
                (GetJsonByRoute(_apiRoute + _alphabetUsersWithSortedTasks ).Result);
        }
        public static UserProjectAndTasks GetUserProjectAndTasks(int id)
        {
            return JsonConvert.DeserializeObject<UserProjectAndTasks>
                (GetJsonByRoute(_apiRoute +  _userProjectAndTasks + id).Result);
        }
        public static ProjectAndTaskStructure GetProjectAndTaskStructure()
        {
            return JsonConvert.DeserializeObject<ProjectAndTaskStructure>
                (GetJsonByRoute(_apiRoute +  _projectAndTaskStructure).Result);
        }
    }
}