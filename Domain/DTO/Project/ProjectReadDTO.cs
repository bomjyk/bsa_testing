using System;

namespace ProjectStructure.Domain.DTO.Project
{
    public class ProjectReadDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        #nullable enable
        public string? Name { get; set; }
        public string? Description { get; set; }
        #nullable disable
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}