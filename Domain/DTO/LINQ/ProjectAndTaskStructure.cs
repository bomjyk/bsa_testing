namespace ProjectStructure.Domain.DTO.LINQ
{
    public class ProjectAndTaskStructure
    {
        public Entities.Project Project { get; set; }
        public Entities.Task LongestProjectTaskByDescription { get; set; }
        public Entities.Task ShortestProjectTaskByName { get; set; }
        public int NumberOfPerformancers { get; set; }
    }
}