using System;

namespace ProjectStructure.Domain.DTO.Team
{
    public class TeamReadDTO
    {
        public int Id { get; set; }
        #nullable enable
        public string? Name { get; set; }
        #nullable disable
        public DateTime CreatedAt { get; set; }
    }
}