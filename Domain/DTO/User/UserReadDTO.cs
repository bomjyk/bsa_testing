using System;

namespace ProjectStructure.Domain.DTO.User
{
    public class UserReadDTO
    {
        public int Id { get; set; }
        #nullable enable
        public int? TeamId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        #nullable disable
        public DateTime BirthDay { get; set; }
        public DateTime RegisteredAt { get; set; }
    }
}