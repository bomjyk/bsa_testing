using System;

namespace ProjectStructure.Domain.Entities
{
    public class Team
    {
        public int Id { get; set; }
        
        #nullable enable
        public string? Name { get; set; }
        #nullable disable
        public DateTime CreatedAt { get; set; }
    }
}