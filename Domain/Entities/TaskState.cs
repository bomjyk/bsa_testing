namespace ProjectStructure.Domain.Entities
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Rejected
    }
}