using System;

namespace ProjectStructure.Domain.Entities
{
    public class Task
    {
        public int Id { get; set; }
        public User Performer { get; set; }
        public Project Project { get; set; }
        #nullable enable
        public string? Name { get; set; }
        public string? Description { get; set; }
        #nullable disable
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}