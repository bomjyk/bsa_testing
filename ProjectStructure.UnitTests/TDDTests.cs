﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Realization;
using ProjectStructure.Domain.DTO.Task;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.UnitTests
{
    public class TDDTest
    {
        private readonly TDDService _service;
        public TDDTest()
        {
            DbContextOptions<ProjectContext> options = new DbContextOptionsBuilder<ProjectContext>()
                            .UseInMemoryDatabase(Guid.NewGuid().ToString())
                            .Options;
            var fakeContext = A.Fake<ProjectContext>(x => x.WithArgumentsForConstructor(
                () => new ProjectContext(options)));
            User user1 = new User()
            {
                Id = 1,
            };
            User user2 = new User()
            {
                Id = 2
            };
            var fakeIQueryableTasks = new List<Task>()
            {
                new Task()
                {
                    Id = 1, CreatedAt = DateTime.Now, FinishedAt = null, Performer = user1
                },
                new Task()
                {
                    Id = 2, CreatedAt = DateTime.Now, FinishedAt = null, Performer = user1
                },
                new Task()
                {
                    Id = 3, CreatedAt = DateTime.Now, FinishedAt = null, Performer = user1
                },
                new Task()
                {
                    Id = 4, CreatedAt = DateTime.Now, FinishedAt = null, Performer = user1
                },
                new Task()
                {
                    Id = 5, CreatedAt = DateTime.Now, FinishedAt = null, Performer = user1
                },
                new Task()
                {
                    Id = 6, CreatedAt = DateTime.Now, FinishedAt = DateTime.Now, Performer = user2
                },
                new Task()
                {
                    Id = 7, CreatedAt = DateTime.Now, FinishedAt = DateTime.Now, Performer = user2
                },
            }.AsQueryable();
            var fakeTasksDbSet = A.Fake<DbSet<Task>>(t => 
                t.Implements(typeof(IQueryable<Task>)));
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).GetEnumerator())
                .Returns(fakeIQueryableTasks.GetEnumerator());
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).Provider)
                .Returns(fakeIQueryableTasks.Provider);
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).Expression)
                .Returns(fakeIQueryableTasks.Expression);
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).ElementType)
                .Returns(fakeIQueryableTasks.ElementType);
            A.CallTo(() => fakeContext.Tasks).Returns(fakeTasksDbSet);
            //Arrange
            var userRpository = new UserRepository(fakeContext);
            var projectRepository = new ProjectRepository(fakeContext);
            var teamsRepository = new TeamRepository(fakeContext);
            var tasksRepository = new TaskRepository(fakeContext);
            var unitOfWork = new UnitOfWork(projectRepository, tasksRepository,
                teamsRepository, userRpository, fakeContext);
            _service = new TDDService(unitOfWork);
        }
        [Fact]
        public void GetUncomplitedTasksByUserId_WhenAllTasksAreUnfinishedAndUserIdIsValid_ThenGetAllTasks()
        {
            
            //Act
            List<ProjectStructure.Domain.Entities.Task> unfinished = (List<Task>)_service.GetUncomplitedTasksByUserId(1);
            //Assert
            Assert.Equal(5,unfinished.Count);
            Assert.All(unfinished, t => Assert.Equal(null, t.FinishedAt));
        }
        [Fact]
        public void GetUncomplitedTasksByUserId_WhenAllTasksAreFinishedAndUserIdIsValid_ThenGetNoneTasks()
        {
            //Act
            List<ProjectStructure.Domain.Entities.Task> unfinished = (List<Task>)_service.GetUncomplitedTasksByUserId(2);
            //Assert
            Assert.Equal(0,unfinished.Count);
        }
        [Fact]
        public void GetUncomplitedTasksByUserId_WhenUserDoesntExist_ThenGetNoneTasks()
        {
            //Act
            List<ProjectStructure.Domain.Entities.Task> unfinished = (List<Task>)_service.GetUncomplitedTasksByUserId(3);
            //Assert
            Assert.Equal(0,unfinished.Count);
        }
    }
}