﻿using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Realization;
using ProjectStructure.Domain.DTO.Task;
using ProjectStructure.Domain.DTO.User;
using ProjectStructure.Domain.Entities;
using Xunit;

namespace ProjectStructure.UnitTests
{
    public class TaskServiceUnitTests
    {
        [Fact]
        public void ChangeTaskAsDone_WhenTaskExists_ThenMethodModifyCalledOnce()
        {
            DbContextOptions<ProjectContext> options = new DbContextOptionsBuilder<ProjectContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var fakeContext = A.Fake<ProjectContext>(x => x.WithArgumentsForConstructor(
                () => new ProjectContext(options)));
            var fakeIQueryableTasks = new List<Task>()
            {
                new Task()
                {
                    Id = 1, CreatedAt = DateTime.Now, FinishedAt = null
                }
            }.AsQueryable();
            var fakeTasksDbSet = A.Fake<DbSet<Task>>(t => 
                t.Implements(typeof(IQueryable<Task>)));
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).GetEnumerator())
                .Returns(fakeIQueryableTasks.GetEnumerator());
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).Provider)
                .Returns(fakeIQueryableTasks.Provider);
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).Expression)
                .Returns(fakeIQueryableTasks.Expression);
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).ElementType)
                .Returns(fakeIQueryableTasks.ElementType);
            A.CallTo(() => fakeContext.Tasks).Returns(fakeTasksDbSet);
            //Arrange
            var userRpository = new UserRepository(fakeContext);
            var projectRepository = new ProjectRepository(fakeContext);
            var teamsRepository = new TeamRepository(fakeContext);
            var tasksRepository = new TaskRepository(fakeContext);
            var unitOfWork = new UnitOfWork(projectRepository, tasksRepository,
                teamsRepository, userRpository, fakeContext);
            var autoMapper = ProjectStructure.Extensions.ServicesExtension.MapperConfiguration().CreateMapper();
            var task = A.Fake<TaskUpdateDTO>();
            task.Id = 1;
            var finished = DateTime.Now;
            task.FinishedAt = finished;
            var service = new TaskService(unitOfWork,
                autoMapper);
            //Act
            service.UpdateTask(task);
            //Assert
            Assert.Equal(finished,fakeTasksDbSet.FirstOrDefault(t => t.Id == 1).FinishedAt);
        }
        [Fact]
        public void ChangeTaskAsDone_WhenTaskDoesntExists_ThenDoesNothing()
        {
            DbContextOptions<ProjectContext> options = new DbContextOptionsBuilder<ProjectContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var fakeContext = A.Fake<ProjectContext>(x => x.WithArgumentsForConstructor(
                () => new ProjectContext(options)));
            var fakeIQueryableTasks = new List<Task>()
            {
                new Task()
                {
                    Id = 1, CreatedAt = DateTime.Now, FinishedAt = null
                }
            }.AsQueryable();
            var fakeTasksDbSet = A.Fake<DbSet<Task>>(t => 
                t.Implements(typeof(IQueryable<Task>)));
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).GetEnumerator())
                .Returns(fakeIQueryableTasks.GetEnumerator());
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).Provider)
                .Returns(fakeIQueryableTasks.Provider);
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).Expression)
                .Returns(fakeIQueryableTasks.Expression);
            A.CallTo(() => ((IQueryable<Task>) fakeTasksDbSet).ElementType)
                .Returns(fakeIQueryableTasks.ElementType);
            A.CallTo(() => fakeContext.Tasks).Returns(fakeTasksDbSet);
            //Arrange
            var userRpository = new UserRepository(fakeContext);
            var projectRepository = new ProjectRepository(fakeContext);
            var teamsRepository = new TeamRepository(fakeContext);
            var tasksRepository = new TaskRepository(fakeContext);
            var unitOfWork = new UnitOfWork(projectRepository, tasksRepository,
                teamsRepository, userRpository, fakeContext);
            var autoMapper = ProjectStructure.Extensions.ServicesExtension.MapperConfiguration().CreateMapper();
            var task = A.Fake<TaskUpdateDTO>();
            task.Id = 2;
            var finished = DateTime.Now;
            task.FinishedAt = finished;
            var service = new TaskService(unitOfWork,
                autoMapper);
            //Act
            service.UpdateTask(task);
            //Assert
            Assert.Equal(null,fakeTasksDbSet.FirstOrDefault(t => t.Id == 1).FinishedAt);
        }
    }
}