using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Xunit;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Realization;
using ProjectStructure.Domain.DTO.User;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Extensions;

namespace ProjectStructure.UnitTests
{
    public class UserServiceUnitTests
    {
        public UserServiceUnitTests()
        {
            
        }
        [Fact]
        public void CreateUser_WhenValid_ThenCreated()
        {
            //Arrange
            DbContextOptions<ProjectContext> options = new DbContextOptionsBuilder<ProjectContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            ProjectContext db = new ProjectContext(options);
            var userRpository = new UserRepository(db);
            var projectRepository = new ProjectRepository(db);
            var teamsRepository = new TeamRepository(db);
            var tasksRepository = new TaskRepository(db);
            var unitOfWork = new UnitOfWork(projectRepository, tasksRepository,
                teamsRepository, userRpository, db);
            var autoMapper = ProjectStructure.Extensions.ServicesExtension.MapperConfiguration().CreateMapper();
            var user = A.Fake<UserCreateDTO>();
            user.TeamId = 2;
            var service = new UserService(autoMapper,
                    unitOfWork,
                    A.Fake<TaskService>(),
                    A.Fake<ProjectService>());
            //Act
            service.RegisterUser(user);
            //Assert
            Assert.Equal(db.Users.ToList().Count(), 1);
        }
        [Fact]
        public void CreateUser_WithoutTeamId_ThenUserCreatedWithTeamNull()
        {
            DbContextOptions<ProjectContext> options = new DbContextOptionsBuilder<ProjectContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var fakeContext = A.Fake<ProjectContext>(x => x.WithArgumentsForConstructor(
                () => new ProjectContext(options)));
            var fakeIQueryableTeams = new List<Team>()
            {
                new Team() {Id = 1, Name = "Swimm pool", CreatedAt = DateTime.Now},
                new Team() {Id = 2, Name = "Attractive pool", CreatedAt = DateTime.Now}
            }.AsQueryable();
            var fakeIQueryableUsers = new List<User>()
            {
                new User() {Id = 1, Email = "mail@mail.com", RegisteredAt = DateTime.Now}
            }.AsQueryable();
            var fakeUserDbSet = A.Fake<DbSet<User>>(u => 
                u.Implements(typeof(IQueryable<User>)));
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).GetEnumerator())
                .Returns(fakeIQueryableUsers.GetEnumerator());
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).Provider)
                .Returns(fakeIQueryableUsers.Provider);
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).Expression)
                .Returns(fakeIQueryableUsers.Expression);
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).ElementType)
                .Returns(fakeIQueryableUsers.ElementType);
            A.CallTo(() => fakeContext.Users).Returns(fakeUserDbSet);
            A.CallTo(() => fakeContext.Users.Add(A<User>.Ignored)).Invokes((User us) =>
            {
                us.FirstName = "Not default name";
            });
            
            var fakeTeamDbSet = A.Fake<DbSet<Team>>(d =>
                d.Implements(typeof(IQueryable<Team>)));
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).GetEnumerator())
                .Returns(fakeIQueryableTeams.GetEnumerator());
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).Provider)
                .Returns(fakeIQueryableTeams.Provider);
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).Expression)
                .Returns(fakeIQueryableTeams.Expression);
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).ElementType)
                .Returns(fakeIQueryableTeams.ElementType);
            A.CallTo(() => fakeContext.Teams).Returns(fakeTeamDbSet);
            //Arrange
            var userRpository = new UserRepository(fakeContext);
            var projectRepository = new ProjectRepository(fakeContext);
            var teamsRepository = new TeamRepository(fakeContext);
            var tasksRepository = new TaskRepository(fakeContext);
            var unitOfWork = new UnitOfWork(projectRepository, tasksRepository,
                teamsRepository, userRpository, fakeContext);
            var autoMapper = ProjectStructure.Extensions.ServicesExtension.MapperConfiguration().CreateMapper();
            var user = A.Fake<UserCreateDTO>();
            user.TeamId = 3;
            var service = new UserService(autoMapper,
                unitOfWork,
                A.Fake<TaskService>(),
                A.Fake<ProjectService>());
            //Act
            service.RegisterUser(user);
            //Assert
            A.CallTo(() => fakeContext.Users.Add(A<User>.That.Matches(u => u.Team.Id == 0))).MustHaveHappened();
        }
        [Fact]
        public void AddUserToTeam_WhenTeamExists_ThenUserTeamChanged()
        {
            DbContextOptions<ProjectContext> options = new DbContextOptionsBuilder<ProjectContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var fakeContext = A.Fake<ProjectContext>(x => x.WithArgumentsForConstructor(
                () => new ProjectContext(options)));
            var fakeIQueryableTeams = new List<Team>()
            {
                new Team() {Id = 1, Name = "Swimm pool", CreatedAt = DateTime.Now},
                new Team() {Id = 2, Name = "Attractive pool", CreatedAt = DateTime.Now}
            }.AsQueryable();
            var fakeIQueryableUsers = new List<User>()
            {
                new User() {Id = 1, Email = "mail@mail.com", RegisteredAt = DateTime.Now}
            }.AsQueryable();
            var fakeUserDbSet = A.Fake<DbSet<User>>(u => 
                u.Implements(typeof(IQueryable<User>)));
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).GetEnumerator())
                .Returns(fakeIQueryableUsers.GetEnumerator());
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).Provider)
                .Returns(fakeIQueryableUsers.Provider);
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).Expression)
                .Returns(fakeIQueryableUsers.Expression);
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).ElementType)
                .Returns(fakeIQueryableUsers.ElementType);
            A.CallTo(() => fakeContext.Users).Returns(fakeUserDbSet);
            
            var fakeTeamDbSet = A.Fake<DbSet<Team>>(d =>
                d.Implements(typeof(IQueryable<Team>)));
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).GetEnumerator())
                .Returns(fakeIQueryableTeams.GetEnumerator());
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).Provider)
                .Returns(fakeIQueryableTeams.Provider);
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).Expression)
                .Returns(fakeIQueryableTeams.Expression);
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).ElementType)
                .Returns(fakeIQueryableTeams.ElementType);
            A.CallTo(() => fakeContext.Teams).Returns(fakeTeamDbSet);
            //Arrange
            var userRpository = new UserRepository(fakeContext);
            var projectRepository = new ProjectRepository(fakeContext);
            var teamsRepository = new TeamRepository(fakeContext);
            var tasksRepository = new TaskRepository(fakeContext);
            var unitOfWork = new UnitOfWork(projectRepository, tasksRepository,
                teamsRepository, userRpository, fakeContext);
            var autoMapper = ProjectStructure.Extensions.ServicesExtension.MapperConfiguration().CreateMapper();
            var user = A.Fake<UserUpdateDTO>();
            user.Id = 1;
            user.TeamId = 1;
            var service = new UserService(autoMapper,
                unitOfWork,
                A.Fake<TaskService>(),
                A.Fake<ProjectService>());
            //Act
            service.UpdateUser(user);
            //Assert
            Assert.Equal(1,fakeIQueryableUsers.FirstOrDefault(u => u.Team.Id == 1).Id);
        }
        [Fact]
        public void AddUserToTeam_WhenTeamDoesntExists_ThenUserTeamEqualNull()
        {
            DbContextOptions<ProjectContext> options = new DbContextOptionsBuilder<ProjectContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var fakeContext = A.Fake<ProjectContext>(x => x.WithArgumentsForConstructor(
                () => new ProjectContext(options)));
            var fakeIQueryableTeams = new List<Team>()
            {
                new Team() {Id = 1, Name = "Swimm pool", CreatedAt = DateTime.Now},
                new Team() {Id = 2, Name = "Attractive pool", CreatedAt = DateTime.Now}
            }.AsQueryable();
            var fakeIQueryableUsers = new List<User>()
            {
                new User() {Id = 1, Email = "mail@mail.com", RegisteredAt = DateTime.Now}
            }.AsQueryable();
            var fakeUserDbSet = A.Fake<DbSet<User>>(u => 
                u.Implements(typeof(IQueryable<User>)));
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).GetEnumerator())
                .Returns(fakeIQueryableUsers.GetEnumerator());
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).Provider)
                .Returns(fakeIQueryableUsers.Provider);
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).Expression)
                .Returns(fakeIQueryableUsers.Expression);
            A.CallTo(() => ((IQueryable<User>) fakeUserDbSet).ElementType)
                .Returns(fakeIQueryableUsers.ElementType);
            A.CallTo(() => fakeContext.Users).Returns(fakeUserDbSet);
            
            var fakeTeamDbSet = A.Fake<DbSet<Team>>(d =>
                d.Implements(typeof(IQueryable<Team>)));
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).GetEnumerator())
                .Returns(fakeIQueryableTeams.GetEnumerator());
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).Provider)
                .Returns(fakeIQueryableTeams.Provider);
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).Expression)
                .Returns(fakeIQueryableTeams.Expression);
            A.CallTo(() => ((IQueryable<Team>) fakeTeamDbSet).ElementType)
                .Returns(fakeIQueryableTeams.ElementType);
            A.CallTo(() => fakeContext.Teams).Returns(fakeTeamDbSet);
            //Arrange
            var userRpository = new UserRepository(fakeContext);
            var projectRepository = new ProjectRepository(fakeContext);
            var teamsRepository = new TeamRepository(fakeContext);
            var tasksRepository = new TaskRepository(fakeContext);
            var unitOfWork = new UnitOfWork(projectRepository, tasksRepository,
                teamsRepository, userRpository, fakeContext);
            var autoMapper = ProjectStructure.Extensions.ServicesExtension.MapperConfiguration().CreateMapper();
            var user = A.Fake<UserUpdateDTO>();
            user.Id = 1;
            user.TeamId = 3;
            var service = new UserService(autoMapper,
                unitOfWork,
                A.Fake<TaskService>(),
                A.Fake<ProjectService>());
            //Act
            service.UpdateUser(user);
            //Assert
            //Todo: This shouldn't work
            Assert.Equal(1,fakeIQueryableUsers.FirstOrDefault(u => u.Team.Id == 3).Id);
        }
    }
}