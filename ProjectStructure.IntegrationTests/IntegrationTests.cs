using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.DAL.Context;
using ProjectStructure.Domain.DTO.Project;
using ProjectStructure.Domain.DTO.Team;
using ProjectStructure.Domain.Entities;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.IntegrationTests
{
    public class IntegrationTests : IClassFixture<CustomWebApplicationFactory<ProjectStructure.Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;
        private HttpClient Client { get; set; }

        public IntegrationTests(CustomWebApplicationFactory<Startup> fixture)
        {
            _factory = fixture;
            Client = fixture.CreateClient(new WebApplicationFactoryClientOptions()
            {
                AllowAutoRedirect = true
            });
        }
        [Fact]
        public async Task CreateProject_ShouldCreate()
        {
            //Arrange
            ProjectCreateDTO projectCreateDto = new ProjectCreateDTO()
            {
                Name = "Test projectStructure",
                AuthorId = 1,
                TeamId =  1,
                Deadline = DateTime.Now,
                Description = "Make unit tests and integrated tests for projectStructure"
            };
            string json = JsonConvert.SerializeObject(projectCreateDto);
            StringContent data = new StringContent(content: json,encoding: Encoding.UTF8,mediaType: "application/json");
            
            
            string projectCreateURI = "/api/Projects/";
            //Act
            var response = await Client.PostAsync(requestUri: projectCreateURI, data);
            //Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK,response.StatusCode);
        }
        [Fact]
        public async Task CreateInvalidProject_ShouldGetError()
        {
            //Arrange
            ProjectCreateDTO projectCreateDto = new ProjectCreateDTO()
            {
                Name = "Test projectStructure",
                AuthorId = 7,
                TeamId =  9,
                Deadline = DateTime.Now,
                Description = "Make unit tests and integrated tests for projectStructure"
            };
            string json = JsonConvert.SerializeObject(projectCreateDto);
            StringContent data = new StringContent(content: json,encoding: Encoding.UTF8,mediaType: "application/json");
            
            
            string projectCreateURI = "/api/Projects/";
            //Act
            var response = await Client.PostAsync(requestUri: projectCreateURI, data);
            //Assert
            Assert.Equal(HttpStatusCode.InternalServerError,response.StatusCode);
        }
        [Fact]
        public async Task RemoveUser_WhenValidId_ThenResponseCodeNoContent()
        {
            //Arrange
            int userIdToRemove = 3;
            string projectCreateURI = $"/api/Projects?id={userIdToRemove}";
            //Act
            var response = await Client.DeleteAsync(requestUri: projectCreateURI);
            //Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.NoContent,response.StatusCode);
        }
        [Fact]
        public async Task RemoveUser_WhenUserDoesntExists_ThenRespondBadRequest()
        {
            //Arrange
            int userIdToRemove = -1;
            string projectCreateURI = $"/api/Projects?id={userIdToRemove}";
            //Act
            var response = await Client.DeleteAsync(requestUri: projectCreateURI);
            //Assert
            Assert.Equal(HttpStatusCode.BadRequest,response.StatusCode);
        }
        [Fact]
        public async Task CreateTeam_WhenTeamNameIsValid_ThenRespondCodeOk()
        {
            //Arrange
            TeamCreateDTO team = new TeamCreateDTO()
            {
                Name = "Pizza boy"
            };
            string projectCreateURI = $"/api/Teams/";
            string json = JsonConvert.SerializeObject(team);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            //Act
            var response = await Client.PostAsync(requestUri: projectCreateURI, content);
            //Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK,response.StatusCode);
        }
        [Fact]
        public async Task CreateTeam_WhenTeamNameIsInvalid_ThenBadRequest()
        {
            //Arrange
            TeamCreateDTO team = new TeamCreateDTO()
            {
                Name = "#5235AS@!@4./53"
            };
            string projectCreateURI = $"/api/Teams/";
            string json = JsonConvert.SerializeObject(team);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            //Act
            var response = await Client.PostAsync(requestUri: projectCreateURI, content);
            //Assert
            Assert.Equal(HttpStatusCode.BadRequest,response.StatusCode);
        }
        [Fact]
        public async Task DeleteTask_WhenTaskIdIsValid_ThenRespondNoContent()
        {
            Client = _factory.WithWebHostBuilder(builder => 
                    builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                         typeof(DbContextOptions<ProjectContext>));

                services.Remove(descriptor);

                services.AddDbContext<ProjectContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryDbForTesting");
                });

                var sp = services.BuildServiceProvider();
                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<ProjectContext>();

                    db.Database.EnsureCreated();

                    try
                    { 
                        DatabaseSeeder.ReinitializeDatabase(db);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"An error occurred seeding the " +
                             "database with test messages. Error: {0}", ex.Message);
                    }
                }
            }))
                .CreateClient(new WebApplicationFactoryClientOptions()
            {
                AllowAutoRedirect = true
            });
            //Arrange
            int taskIdToDelete = 1;
            string projectCreateURI = $"/api/Tasks?id={taskIdToDelete}";
            //Act
            var response = await Client.DeleteAsync(requestUri: projectCreateURI);
            //Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.NoContent,response.StatusCode);
        }
        [Fact]
        public async Task DeleteTask_WhenTaskDoesntExists_ThenRespondBadRequest()
        {
            //Arrange
            int taskIdToDelete = -1;
            string projectCreateURI = $"/api/Tasks?id={taskIdToDelete}";
            //Act
            var response = await Client.DeleteAsync(requestUri: projectCreateURI);
            //Assert
            Assert.Equal(HttpStatusCode.BadRequest,response.StatusCode);
        }
        [Fact]
        public async Task GetTasksByUser_WhenUserExist_ThenRespondOkAndFirstProject1Task()
        {
            //Arrange
            int userId = 1;
            string projectCreateURI = $"/api/Linq/taskByUserId/id?id={userId}";
            //Act
            using (var response = await Client.GetAsync(requestUri: projectCreateURI)) {
                //Assert
                response.EnsureSuccessStatusCode();
                Assert.Equal(HttpStatusCode.OK,response.StatusCode);
                using (HttpContent responseContent = response.Content)
                {
                    Dictionary<Project, int> tasks = JsonConvert.DeserializeObject<KeyValuePair<Project, int>[]>
                        ((await response.Content.ReadAsStringAsync()), new JsonSerializerSettings()).ToDictionary(
                        d => d.Key, d => d.Value);
                    Assert.Equal(1, tasks.First(p => p.Key.Id == 1).Value);
                    Assert.Equal(0, tasks.First(p => p.Key.Id == 2).Value);
                }
            }
        }
        [Fact]
        public async Task GetTasksByUser_WhenUserDoesntExist_ThenRespondOkAndZeroTasks()
        {
            //Arrange
            int userId = -1;
            string projectCreateURI = $"/api/Linq/taskByUserId/id?id={userId}";
            //Act
            using (var response = await Client.GetAsync(requestUri: projectCreateURI)) {
                //Assert
                response.EnsureSuccessStatusCode();
                Assert.Equal(HttpStatusCode.OK,response.StatusCode);
                using (HttpContent responseContent = response.Content)
                {
                    Dictionary<Project, int> tasks = JsonConvert.DeserializeObject<KeyValuePair<Project, int>[]>
                        ((await response.Content.ReadAsStringAsync()), new JsonSerializerSettings()).ToDictionary(
                        d => d.Key, d => d.Value);
                    Assert.Equal(0, tasks.First(p => p.Key.Id == 1).Value);
                    Assert.Equal(0, tasks.First(p => p.Key.Id == 2).Value);
                }
            }
        }
    }
}