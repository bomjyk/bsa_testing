﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProjectStructure.DAL.Context;


namespace ProjectStructure.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup>
        where TStartup: class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                         typeof(DbContextOptions<ProjectContext>));

                services.Remove(descriptor);

                services.AddDbContext<ProjectContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryDbForTesting");
                });

                var sp = services.BuildServiceProvider();
                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<ProjectContext>();

                    db.Database.EnsureCreated();

                    try
                    { 
                        DatabaseSeeder.InitializeDatabase(db);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"An error occurred seeding the " +
                             "database with test messages. Error: {0}", ex.Message);
                    }
                }
            });
        }
    }
}