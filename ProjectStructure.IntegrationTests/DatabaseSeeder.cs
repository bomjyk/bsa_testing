﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.DAL.Context;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.IntegrationTests
{
    public class DatabaseSeeder
    {
        private readonly ProjectContext _context;
        public DatabaseSeeder(ProjectContext context)
        {
            _context = context;
        } 
        public static void InitializeDatabase(ProjectContext _context)
        {
            DatabaseInitializator(_context);
        }

        public static void ReinitializeDatabase(ProjectContext _context)
        {
            DatabaseFlush(_context);
            DatabaseInitializator(_context);
        }
        private static void DatabaseFlush(ProjectContext context)
        {
            context.Teams.RemoveRange(getTeamRange());
            context.Users.RemoveRange(getUserRange(context));
            context.Projects.RemoveRange(getProjectRange(context));
            context.Tasks.RemoveRange(getTaskRange(context));
            context.SaveChanges();
        }
        private static void DatabaseInitializator(ProjectContext context)
        {
            context.Teams.AddRange(getTeamRange());
            context.Users.AddRange(getUserRange(context));
            context.Projects.AddRange(getProjectRange(context));
            context.Tasks.AddRange(getTaskRange(context));
            context.SaveChanges();
        }

        private static IEnumerable<Task> getTaskRange(ProjectContext context)
        {
            return new[]
            {
                new Task()
                {
                    Description = "Clean kot",
                    Name = "Clean a Cat",
                    Performer = context.Users.FirstOrDefault(u => u.Id == 1),
                    Project = context.Projects.FirstOrDefault(p => p.Id == 1),
                    State = (TaskState) 3,
                    CreatedAt = new DateTime(2021, 06, 30),
                    FinishedAt = null,
                },
                new Task()
                {
                    Description = "Clean kot",
                    Name = "Clean a Cat",
                    Performer = context.Users.FirstOrDefault(u => u.Id == 1),
                    Project = context.Projects.FirstOrDefault(p => p.Id == 1),
                    State = (TaskState) 3,
                    CreatedAt = new DateTime(2021, 06, 30),
                    FinishedAt = null,
                },
                new Task()
                {
                    Description = "Clean kot",
                    Name = "Clean a Cat",
                    Performer = context.Users.FirstOrDefault(u => u.Id == 1),
                    Project = context.Projects.FirstOrDefault(p => p.Id == 1),
                    State = (TaskState) 3,
                    CreatedAt = new DateTime(2021, 06, 30),
                    FinishedAt = new DateTime(2021, 06, 30),
                },
                new Task()
                {
                    Description = "Clean Pes",
                    Name = "Clean a Dog",
                    Performer = context.Users.FirstOrDefault(u => u.Id == 2),
                    Project = context.Projects.FirstOrDefault(p => p.Id == 1),
                    State = (TaskState) 3,
                    CreatedAt = new DateTime(2021, 06, 30),
                    FinishedAt = new DateTime(2021, 10, 20),
                }
            };
        }
        private static IEnumerable<Project> getProjectRange(ProjectContext context)
        {
            return new[]
            {
                new Project()
                {
                    Author = context.Users.FirstOrDefault(u => u.Id == 1),
                    Team = context.Teams.FirstOrDefault(t => t.Id == 1),
                    Name = "BestProject",
                    Description = "Best ever made project",
                    Deadline = new DateTime(2030, 10, 10),
                    CreatedAt = new DateTime(2021, 06, 30)
                },
                new Project()
                {
                    Author = context.Users.FirstOrDefault(u => u.Id == 2),
                    Team = context.Teams.FirstOrDefault(t => t.Id == 2),
                    Name = "BestProject",
                    Description = "Best ever made project",
                    Deadline = new DateTime(2030, 10, 10),
                    CreatedAt = new DateTime(2021, 06, 30)
                },
            };
        }
        private static IEnumerable<User> getUserRange(ProjectContext context)
        {
            return new[]
            {
                new User()
                {
                    FirstName = "Oleg",
                    LastName = "Myhailuk",
                    Email = "oleg.myhailuk@gmail.com",
                    BirthDay = new DateTime(1990, 06, 28),
                    RegisteredAt = new DateTime(2021, 06, 30),
                    Team = context.Teams.FirstOrDefault(t => t.Id == 1)
                },
                new User()
                {
                    FirstName = "Misha",
                    LastName = "Myhailuk",
                    Email = "misha.myhailuk@gmail.com",
                    BirthDay = new DateTime(1999, 01, 11),
                    RegisteredAt = new DateTime(2021, 06, 30),
                    Team = context.Teams.FirstOrDefault(t => t.Id == 1)
                },
                new User()
                {
                    FirstName = "Olena",
                    LastName = "Chorna",
                    Email = "olena.chorna@gmail.com",
                    BirthDay = new DateTime(1970, 10, 20),
                    RegisteredAt = new DateTime(2021, 06, 30),
                    Team = context.Teams.FirstOrDefault(t => t.Id == 2)
                },
                new User()
                {
                    FirstName = "Viktor",
                    LastName = "Bur",
                    Email = "viktor.bur@gmail.com",
                    BirthDay = new DateTime(1960, 11, 02),
                    RegisteredAt = new DateTime(2021, 06, 30),
                    Team = context.Teams.FirstOrDefault(t => t.Id == 2)
                },
                new User()
                {
                    FirstName = "Yaroslav",
                    LastName = "Vitok",
                    Email = "yaroslav.vitok@gmail.com",
                    BirthDay = new DateTime(2001, 01, 14),
                    RegisteredAt = new DateTime(2021, 06, 30),
                    Team = context.Teams.FirstOrDefault(t => t.Id == 3)
                }
            };
        }
        private static IEnumerable<Team> getTeamRange()
        {
            return new[]
            {
                new Team()
                {
                    Name = "McDonalds",
                    CreatedAt = DateTime.Now
                },
                new Team()
                {
                    Name = "Carnegie",
                    CreatedAt = DateTime.Now
                },
                new Team()
                {
                    Name = "LosAngelesLakers",
                    CreatedAt = DateTime.Now
                }
            };
        }
    }
}