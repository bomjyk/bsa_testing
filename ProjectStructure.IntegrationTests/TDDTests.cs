﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.DAL.Context;
using ProjectStructure.Domain.DTO.Project;
using ProjectStructure.Domain.DTO.Team;
using ProjectStructure.Domain.Entities;
using Xunit;
namespace ProjectStructure.IntegrationTests
{
    public class TDDTests : IClassFixture<CustomWebApplicationFactory<ProjectStructure.Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;
        private HttpClient Client { get; set; }

        public TDDTests(CustomWebApplicationFactory<Startup> fixture)
        {
            _factory = fixture;
            Client = fixture.CreateClient(new WebApplicationFactoryClientOptions()
            {
                AllowAutoRedirect = true
            });
        }
        [Fact]
        public async System.Threading.Tasks.Task GetCollection_WhenUserExists_ThenReturnCollecitonWith2Elements()
        {
            //Arrange
            int userId = 1;
            string projectCreateURI = $"/api/TDD?userId={userId}";
            //Act
            var response = await Client.GetAsync(requestUri: projectCreateURI);
            //Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK,response.StatusCode);
            List<Task> unfinishedTasks = JsonConvert.DeserializeObject<List<Task>>((
                await response.Content.ReadAsStringAsync()), new JsonSerializerSettings());
            Assert.Equal(2, unfinishedTasks.Count);
        }
        [Fact]
        public async System.Threading.Tasks.Task GetCollection_WhenUserDoesntExists_ReturnCollectionWithZeroElements()
        {
            //Arrange
            int userId = 5;
            string projectCreateURI = $"/api/TDD?userId={userId}";
            //Act
            var response = await Client.GetAsync(requestUri: projectCreateURI);
            //Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK,response.StatusCode);
            List<Task> unfinishedTasks = JsonConvert.DeserializeObject<List<Task>>((
                await response.Content.ReadAsStringAsync()), new JsonSerializerSettings());
            Assert.Equal(0, unfinishedTasks.Count);
        }
    }
}