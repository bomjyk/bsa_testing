using System.Collections.Generic;
using ProjectStructure.Domain.DTO.Project;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IProjectService
    {
        void CreateProject(ProjectCreateDTO item);
        void UpdateProject(ProjectUpdateDTO item);
        void DeleteProject(int id);
        ProjectReadDTO GetProject(int id);
        IEnumerable<ProjectReadDTO> GetAllProjects();
    }
}