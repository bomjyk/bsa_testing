using System.Collections.Generic;
using ProjectStructure.Domain.DTO.Task;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITaskService
    {
        void AddNewTask(TaskCreateDTO item);
        void UpdateTask(TaskUpdateDTO item);
        TaskReadDTO GetTask(int id);
        IEnumerable<TaskReadDTO> GetAllTasks();
        void DeleteTask(int id);
    }
}