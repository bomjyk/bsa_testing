using System.Collections.Generic;
using ProjectStructure.Domain.DTO.Team;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITeamService
    {
        void CreateTeam(TeamCreateDTO team);
        void DeleteTeam(int id);
        void UpdateTeam(TeamUpdateDTO team);
        TeamReadDTO GetTeam(int id);
        IEnumerable<TeamReadDTO> GetAllTeams();
    }
}