﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.BLL.Services
{
    public class TDDService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TDDService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ICollection<Task> GetUncomplitedTasksByUserId(int userId)
        {
            return _unitOfWork.Tasks.GetAll()
                .Where(t => t.Performer.Id == userId)
                .Where(t => t.FinishedAt == null)
                .ToList();
        }
    }
}